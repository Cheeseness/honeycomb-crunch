# Honeycomb CRUNCH
Honeycomb CRUNCH is an example top down scrolling game made using the MIT licenced [Godot engine](http://godotengine.org/). This game was initially made for a talk on making games for the Tasmanian Linux User Group in May 2019. The game is playable, but it still has a number of rough edges. Wear gloves when handling! In this file, you can find:

* Licence information for code, assets and third party assets
* Instructions for playing the game
* Instructions for editing and/or running the game 
* Notes on how to navigate the game's codebase and resources
* Some helpful/related URLs

I hope you find something useful from playing Honeycomb CRUNCH and exploring its code!

-Cheese


## Licences
All source code within this repository is licenced under the [GNU Lesser General Public Licence 3.0](http://www.gnu.org/licenses/lgpl.txt), allowing you to use, share, and modify any or all parts of the codebase so long as they retain this licence and that you make source changes available to anybody you distribute modified versions to. See the file COPYING for the full licence. Yay!

Excluding the files noted below, all assets are were created by Josh "Cheeseness" Bush and released for use via the [Creative Commons Attribution 4.0 licence](https://creativecommons.org/licenses/by/4.0/). Under these terms, you are free to do whatever you like with the assets in this repository so long as attribution is given (the items noted below are subject to their own licence requirements, though). Yay!

### Third Party Sound Effects
The timing, length and volume of these samples has been modified to suit Honeycomb CRUNCH.

* [bang-04-clean](http://freesound.org/people/Eelke/sounds/170424/) Eelke, available via [Creative Commons 0](http://creativecommons.org/publicdomain/zero/1.0/)
* [HQ Explosion](http://freesound.org/people/Quaker540/sounds/245372/) Quaker540, available via [Creative Commons 0](http://creativecommons.org/publicdomain/zero/1.0/)
* [GUI Sound Effects_031](https://opengameart.org/content/gui-sound-effects-4) Jesús Lastra, licenced under Creative Commons: [Attribution-ShareAlike 3.0](https://creativecommons.org/licenses/by-sa/3.0/)

### Third Party Music
This music track has been used in Honeycomb CRUNCH.

* [Super Friendly](http://incompetech.com) Kevin MacLeod , licenced under Creative Commons: [Attribution 3.0](http://creativecommons.org/licenses/by/3.0/)


### Third Party Fonts
This font has been used in Honeycomb CRUNCH.

* [Dimbo](https://www.dafont.com/dimbo.font) Gustavo Paz, licenced under Creative Commons: [Attribution-ShakeAlike 3.0](https://creativecommons.org/licenses/by-sa/3.0/)


## Play Instructions
Avoid hexagons while touching flowers and collecting pollen. Note that you steer the bee to control its speed and direction of movement, not to control its absolute position.

* Up/Down, Left stick Y, Mouse Y: Angle up/down
* Left/Right, Left stick X, Mouse X: Angle left/right


## Running The Game
If you have downloaded a pre-packaged version of the game for Linux, Mac OS or Windows from the Honeycomb CRUNCH [Itch.io page](http://cheeseness.itch.io/honeycomb-crunch). Extract the relevant archive for your platform and run it!

If you have downloaded [the game's source](https://gitlab.com/Cheeseness/honeycomb-crunch), you can select the "godot" folder from the _Godot Project Manager_ and click the _Run_ button on the right to run it.


## Editing The Game In Godot
If you have downloaded [the game's source](https://gitlab.com/Cheeseness/honeycomb-crunch) to experiment with yourself, you can select the _godot_ folder from the _Godot Project Manager_ and click the _Edit_ button on the right to open Honeycomb CRUNCH in the Godot editor.

## Understanding The Godot Project and Source Code
The Tiny Chopper Raceway Godot project can be found in the _godot_ folder. There are very few code comments (this may change in the future!). Here are some notes to help you navigate the codebase and resources.


### Folder Structure

* The **fonts** folder contains the font used by the game
* The **json** folder contains json formatted data files used by the game
* The **models** folder contains Collada exports of the 3D models that the game uses in DAE format, and as well as associated material files
* The **scenes** folder contains Godot scenes that represent discrete GUI, gameplay and graphical elements
* The **scripts** folder contains all of the gdscript code files
* The **sounds** folder contains all of the sound samples used by the game in WAV format
* The **sprites** folder contains all the 2D sprites used by the game in PNG format

### Code Files
The game's code is stored in the _godot/scripts_ folder and consists of the following scripts:

* **bee_handler.gd** includes input handling and collision detection code for player controlled bees
* **credits.gd** includes code for assembling and displaying a scrolling credits list at runtime
* **explosion.gd** includes code for automatically removing bee explosion effects when they're done
* **flower.gd** includes code for handling flower movement and behaviour, as well as a type identifier referenced by bee_handler.gd during collisions
* **hex.gd** includes code for handling hexagon movement and behaviour, as well as a type identifier referenced by bee_handler.gd during collisions
* **pollen.gd** includes code for handling pollen behaviour, as well as a type identifier referenced by bee_handler.gd during collisions
* **flower_explosion.gd** includes code for automatically removing flower petal explosion effects when they're done
* **game_over.gd** includes code for displaying and handling input from the game over screen
* **hex_spawner.gd** includes code for instantiating bees, hexes, flowers, and pollen, as well as tracking player lives, pollen and playtime. It also contains a number of utility functions used by other scripts
* **hud.gd** includes code for displaying and updating HUD elements
* **menu.gd** includes code for animating, displaying, and handling input from the main menu screen


### Asset Sources
The game's 2D, 3D, and audio assets are stored in the sprites, models, and sounds folders respectively in editable formats.


## Helpful URLs
* [More information on Honeycomb CRUNCH](http://cheeseness.itch.io/honeycomb-crunch)
* [Honeycomb CRUNCH source code](https://gitlab.com/Cheeseness/honeycomb-crunch)
* [Godot Documentation](http://docs.godotengine.org/)
* [Support Cheese Making Games](http://patreon.com/cheeseness)
