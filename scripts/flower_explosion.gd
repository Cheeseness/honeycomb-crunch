extends Spatial

#Declare petal variables
var components = [] #A list of the current particle effects
var flowerType = 1 #The type of the flower that is exploding (set in hex_spawner.gd)

func _ready():
	#Loop through all child nodes looking for Particles nodes
	for child in get_children():
		if child is Particles:
			#Append the particles node to our list
			components.append(child)

			#Set the colour gradient used for the petals based on the flower type
			if flowerType == 1:
				child.process_material.color_ramp.gradient = preload("res://sprites/flower_explosion_gradient1.tres")
			elif flowerType == 2:
				child.process_material.color_ramp.gradient = preload("res://sprites/flower_explosion_gradient2.tres")
			elif flowerType == 3:
				child.process_material.color_ramp.gradient = preload("res://sprites/flower_explosion_gradient3.tres")

			#Set the particle system emitting particles
			child.emitting = true

	#Play the flower pop sound
	get_node("AudioStreamPlayer").play()


#This built-in function is called regularly by the engine
func _process(_delta):
	#If any particle effects are still emitting, wait, otherwise remove the whole lot from the scene tree
	var timeToDie = true
	for component in components:
		if component.emitting == true:
			timeToDie = false
	if timeToDie:
		queue_free()