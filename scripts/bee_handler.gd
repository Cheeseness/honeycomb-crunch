extends Spatial

#Declare some bee variables
var xSensitivity = 0.01 #X axis sensitivity for mouse bee steering
var ySensitivity = 0.01 #Y axis sensitivity for mouse bee steering
var deadzone = 0.25 #Dead zone for mouse bee steering
var deadzoneGamepad = 0.1 #Dead zone for gamepad steering
var inputDirection = Vector2(0, 0) #Tracks an abstracted representation of player input as a vector representing heading and speed
var edgeBuffer = 2 #A buffer distance around the edge of the screen that the bee can't move into
var moveSpeed = 5 #The bee's movement speed
var spawnHeight #The height that the bee spawned above the play area (set in hex_spawner.gd)
var descendSpeed = 2 #The speed that the bee moves towards the play area after spawning
var camera #A variable that we can access the camera via


func _ready():
	#Grab a reference to the camera that we can refer to later
	camera = get_parent().get_node("Camera")

	#Complex changes to an a scene that inherits an imported 3D model like the bee here will break that inheritance and stop it updating when the model is updated, so let's programmatically apply the changes we want
	#Get the shape out of the automatically generated bee hitbox static body
	var shape = get_node("BeeHitbox/shape")

	#Remove the shape from the static body so that it doesn't get freed when the static body is removed
	get_node("BeeHitbox").remove_child(shape)

	#Remove the static body. This will stop it from triggering its own collision events.
	get_node("BeeHitbox").queue_free()

	#Instantiate and a new Area for handling collisions, add the hitbox shape to it, and then append the Area to the scene tree
	var collisionArea = Area.new()
	collisionArea.add_child(shape)
	add_child(collisionArea)

	#Connect the collision Area's body_entered signal to the on_collision function
	var temp = collisionArea.connect("body_entered", self, "on_collision")

	#These two lines do nothing
	#Godot marks unused variables as errors. Since type is only used by the bee_handler script, we need to do something with it here to suppress that error.
	if temp:
		pass

	#Instantiate a new Area for handling gravity influence on pollen and add a new sphere collision shape to it
	var gravityArea = Area.new()
	var gravityShape = CollisionShape.new()
	var sphereShape = SphereShape.new()

	#Set the sphere shape's radius and add it to the Area
	sphereShape.set_radius(20)
	gravityShape.set_shape(sphereShape)
	gravityArea.add_child(gravityShape)

	#Set the gravity settings for the Area so that it'll pulll the pollens all nice like
	gravityArea.set_gravity(40)
	gravityArea.set_gravity_vector(Vector3(0, 0, 0))
	gravityArea.set_gravity_is_point(true)
	gravityArea.set_gravity_distance_scale(0.3)

	#Make the gravity settings override any existing gravity settings (without this it won't do anything!)
	gravityArea.set_space_override_mode(Area.SPACE_OVERRIDE_REPLACE)

	#Add the gravity Area to the scene tree
	add_child(gravityArea)

	#Instantiate and configure a tween to scale the bee as it spawns in to simulate perspective
	#This doesn't make any sense since we have an ortho camera, but it looks nice
	var spawnTween = Tween.new()
	spawnTween.interpolate_property(self, "scale", Vector3(4, 4, 4), Vector3(1, 1, 1), 2.5, Tween.TRANS_LINEAR, Tween.EASE_IN)

	#Add the tween to the scene tree and start it
	add_child(spawnTween)
	spawnTween.start()

	#Check for any current input states that might have fired their events before this bee existed
	check_current_input()


#This function is called when a physics body enters the collision Area
func on_collision(body):
	#TODO: Make type an enum declared somewhere centralised

	#With pollen, the "type" is stored in the rigid body node that is passed in here
	if "type" in body:
		if body.type == 2:
			#Only collect the pollen (type 2) if gravTime has elapsed
			if body.lifeTime >= body.gravTime:
				get_parent().collect_pollen(body)
	#With all other collision objects, the "type" is stored in the parent of the physics body node that is passed in here
	elif "type" in body.get_parent():
		if body.get_parent().type == 0:
			#If we hit a hex (type 0), blow up the bee
			get_parent().bee_boom()
		elif body.get_parent().type == 1:
			#If we hit a flower (type 1), spawn some pollen and blow the flower up
			get_parent().add_pollen(body.get_global_transform(), body.get_parent().flowerType)
			get_parent().remove_flower(body.get_parent())


#This built-in function is called regularly by the engine
func _process(delta):
	#Store the bee's current position
	var currentPosition = get_global_transform()

	#Declare a variable that we'll add an offset to if we're still on our spawn descent
	var landOffset = Vector3(0, 0, 0)

	#If the bee's position on the Y axis isn't zero, we're still descending
	if currentPosition.origin.y != 0:
		#Reduce the spawn height, and then set the land offset so that we move "down" along the Y axis toward the play area and "up" along the Z axis away from the bottom of the screen
		spawnHeight = spawnHeight - spawnHeight * delta
		landOffset = Vector3(0, -descendSpeed * delta, -spawnHeight * delta)

	#Work out a scaled edge buffer that respects camera size
	var tempBuffer = edgeBuffer * camera.size

	#Make a copy of the player input and appply deadzones to it
	var inputTemp = inputDirection
	if inputTemp.x < deadzone && inputTemp.x > -deadzone:
		inputTemp.x = 0
	if inputTemp.y < deadzone && inputTemp.y > -deadzone:
		inputTemp.y = 0

	#Update the copy of the current position with player input, and get the resulting point in 2D window coordinates
	currentPosition.origin = get_global_transform().origin + Vector3(inputTemp.x * moveSpeed * delta, 0, inputTemp.y * moveSpeed * delta)
	var chicken = camera.unproject_position(currentPosition.origin)

	#If the resulting position moves the bee into the edge buffer along an axis, set the input for that axis to zero and update the copy of the current position
	if !get_viewport().get_visible_rect().grow(-tempBuffer).has_point(chicken):
		if chicken.x < 0 + tempBuffer ||  chicken.x > get_viewport().get_visible_rect().size.x - tempBuffer:
			inputTemp.x = 0
		if chicken.y < 0 + tempBuffer || chicken.y > get_viewport().get_visible_rect().size.y - tempBuffer:
			inputTemp.y = 0
		currentPosition.origin = get_global_transform().origin + Vector3(inputTemp.x * moveSpeed * delta, 0, inputTemp.y * moveSpeed * delta)

	#Only update the bee position if we're not dying (it looks weird when the dead bee is flying out of the explosion under full control)
	if !get_parent().isDying:
		#If we're still descending, apply the landing offset
		if currentPosition.origin.y != 0:
			currentPosition.origin = currentPosition.origin + landOffset
			#If we've descended too far, just set Y to zero, and that'll get picked up in the next _process() call
			if currentPosition.origin.y < 0:
				currentPosition.origin.y = 0

		#Set the new bee position
		set_global_transform(currentPosition)

	#Rotate the bee based on input (regardless of dead zones or whether we actually move or not)
	rotation = Vector3(PI / 4 * inputDirection.y, 0, PI / 4 * -inputDirection.x)


#This function checks for any current input states that might have fired their events before this bee existed
func check_current_input():
	#Set input direction based on existing keyboard presses
	if Input.is_action_pressed("ui_down"):
		inputDirection.y = int(inputDirection.y + 1)
	if Input.is_action_pressed("ui_up"):
		inputDirection.y = int(inputDirection.y - 1)
	if Input.is_action_pressed("ui_right"):
		inputDirection.x = int(inputDirection.x + 1)
	if Input.is_action_pressed("ui_left"):
		inputDirection.x = int(inputDirection.x - 1)

	#If we know what the last device to give joystick input was, set input direction based on its current state
	if get_parent().lastJoypadDeviceID > -1:
		var axisValue = Input.get_joy_axis(get_parent().lastJoypadDeviceID, 0) 
		if abs(axisValue) > deadzoneGamepad:
			inputDirection.x = axisValue
		axisValue = Input.get_joy_axis(get_parent().lastJoypadDeviceID, 1) 
		if abs(axisValue) > deadzoneGamepad:
			inputDirection.y = axisValue


#This built-in function is called whenever there are input events that aren't already handled
func _unhandled_input(event):
	#If the event is mouse movement and we're not dying, set the input direction to a unit vector (vector length of 1) that represents current input
	if event is InputEventMouseMotion:
		if !get_parent().isDying:
			inputDirection = inputDirection + Vector2(event.relative.x * xSensitivity, event.relative.y * ySensitivity)
			inputDirection = inputDirection.clamped(1.0)
			get_tree().set_input_as_handled()

	#If the event is a key press and not an echo, set the input direction to a unit vector (vector length of 1) that represents current input
	elif event is InputEventKey && !event.is_echo():
		#if (event.scancode == KEY_DOWN && event.pressed) || (event.scancode == KEY_UP && !event.pressed):
		if (event.is_action_pressed("ui_down") || event.is_action_released("ui_up")):
			inputDirection.y = int(inputDirection.y + 1)
			get_tree().set_input_as_handled()
		#if (event.scancode == KEY_DOWN && !event.pressed) || (event.scancode == KEY_UP && event.pressed):
		if (event.is_action_pressed("ui_up") || event.is_action_released("ui_down")):
			inputDirection.y = int(inputDirection.y - 1)
			get_tree().set_input_as_handled()
		#if (event.scancode == KEY_RIGHT && event.pressed) || (event.scancode == KEY_LEFT && !event.pressed):
		if (event.is_action_pressed("ui_right") || event.is_action_released("ui_left")):
			inputDirection.x = int(inputDirection.x + 1)
			get_tree().set_input_as_handled()
		#if (event.scancode == KEY_RIGHT && !event.pressed) || (event.scancode == KEY_LEFT && event.pressed):
		if event.is_action_pressed("ui_left") || event.is_action_released("ui_right"):
			inputDirection.x = int(inputDirection.x - 1)
			get_tree().set_input_as_handled()
		inputDirection = inputDirection.clamped(1.0)

	#If the event is joystick movement on axis 0 or 1, apply that to input direction (respecting gamepad deadzone)
	elif event is InputEventJoypadMotion:
		if event.axis == 0:
			if abs(event.axis_value) < deadzoneGamepad:
				inputDirection.x = 0
			else:
				inputDirection.x = event.axis_value
			get_parent().lastJoypadDeviceID = event.get_device()
			get_tree().set_input_as_handled()
		if event.axis == 1:
			if abs(event.axis_value) < deadzoneGamepad:
				inputDirection.y = 0
			else:
				inputDirection.y = event.axis_value
			get_parent().lastJoypadDeviceID = event.get_device()
			get_tree().set_input_as_handled()