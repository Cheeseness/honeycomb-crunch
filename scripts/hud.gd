extends Control

#Declare some variables that we'll use to keep track of and manipulate HUD elements
var pollen #The text label that displays current pollen
var time #The text label that displays current play time
var stage #The text label that displays the current stage
var beeIcons = [] #A list of instantiated bee icons
var beeIcon = preload("res://sprites/bee.png") #The icon we'll use to represent available bees
var pollenTween #A tween to animate the pollen counter
var pollenPos #The initial position of the pollen counter
var stageTween #A tween to animate the stage display
var stagePos #The initial psoition of the stage display
var beeSound #An AudioStreamPlayer that will play a sound when a new bee is awarded



func _ready():
	#Assign variables for referencing and maniupulating HUD elements
	setup_hud()

	#Give the time counter an initial value
	time.set_text(get_parent().get_time_string(0))

	#Instiantiate some tweens and add them to the scene tree
	pollenTween = Tween.new()
	add_child(pollenTween)
	stageTween = Tween.new()
	add_child(stageTween)


#This function assigns variables for referencing and maniupulating HUD elements
func setup_hud():
	#TODO: Call this whenever the window size is changed
	stage = get_node("VBoxContainer/Stage")
	stagePos = stage.get_position()
	pollen = get_node("VBoxContainer/HBoxContainer/PollenContainer/Pollen")
	pollenPos = pollen.get_position()
	time = get_node("VBoxContainer/HBoxContainer/Time")
	beeSound = get_node("AudioStreamPlayer")


#This function updates the time counter
func update_time(newTime):
	time.set_text(newTime)


#This function updates the pollen counter
func update_pollen(newPollen):
	#Set the text for the pollen counter
	pollen.set_text("Pollen: " + str(newPollen))

	#Set up a tween to make the pollen counter bigger, and another to adjust its position so that it more or less stays centred while doing so
	pollenTween.interpolate_property(pollen, "rect_scale", pollen.get_scale(), Vector2(1.125, 1.125), 0.125, Tween.TRANS_LINEAR, Tween.EASE_IN)
	pollenTween.interpolate_property(pollen, "rect_position", pollen.get_position(), pollenPos - pollen.get_size() * 0.125, 0.125, Tween.TRANS_LINEAR, Tween.EASE_IN)

	#Set up a tween to return the pollen to normal size after a delay, and another to adjust its position so that it more or less stays centred while doing so
	pollenTween.interpolate_property(pollen, "rect_scale", pollen.get_scale(), Vector2(1, 1), 0.25, Tween.TRANS_LINEAR, Tween.EASE_IN, 0.125)
	pollenTween.interpolate_property(pollen, "rect_position", pollen.get_position(), pollenPos, 0.25, Tween.TRANS_LINEAR, Tween.EASE_IN, 0.125)

	#Start those tweens
	pollenTween.start()


#This function adds a bee to the HUD
func add_bee(playSound = false):
	#Instantiate a new TextureRect and set its texture to the bee icon
	var newBee = TextureRect.new()
	newBee.texture = beeIcon

	#Add the icon to the scene tree and to our list of current icons
	get_node("VBoxContainer/HBoxContainer/BeeContainer").add_child(newBee)
	beeIcons.append(newBee)

	#If desired, play the new bee sound
	if playSound:
		beeSound.play()
	#TODO: Add a tween to animate new bees being added

#This function removes a bee from the HUD
func remove_bee():
	if beeIcons.size() > 0:
		#Remove one of the bee icons from the scene tree and remove it from our list of current icons
		beeIcons.back().queue_free()
		beeIcons.pop_back()
		#TODO: Add a tween to animate bees being removed


#This function flashes the current stage on screen after a specified delay
func show_stage(stageNumber, delay = 0):
	#Our counter is zero based, so add 1 when displaying it
	stage.set_text("Stage " + str(stageNumber + 1))

	#Set up tweens to make the stage text bigger, another to adjust its position so that it more or less stays centred while doing so, and another to make it fade out
	stageTween.interpolate_property(stage, "rect_scale", Vector2(1, 1), Vector2(3, 3), 1.25, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT, delay)
	stageTween.interpolate_property(stage, "rect_position", stage.get_position(), stagePos - stage.get_size(), 1.25, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT, delay)
	stageTween.interpolate_property(stage, "modulate", Color(1, 1, 1, 1), Color(1, 1, 1, 0), 1, Tween.TRANS_SINE, Tween.EASE_IN_OUT, delay)

	#Set them tweens going
	stageTween.start()