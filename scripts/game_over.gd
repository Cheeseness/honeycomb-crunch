extends Control

#Declare some variables that we'll use to keep track of and populate UI elements
var stageLabel #The text label that displays the current stage
var pollenLabel #The text label that displays pollen collected
var timeLabel #The text label that displays the run's duration
var scoreLabel #The text label that displays the final score
var lastscoreLabel #The text label that displays the previous score
var highscoreLabel #The text label that displays the highest recorded score
var historyBox #The text label that displays the current stage
var history #An array of historic score values for charting


func _ready():
	#Assign some variables for referencing and maniupulating HUD elements
	stageLabel = get_node("CenterContainer/VBoxContainer/Stage")
	pollenLabel = get_node("CenterContainer/VBoxContainer/Pollen")
	timeLabel = get_node("CenterContainer/VBoxContainer/Time")
	scoreLabel = get_node("CenterContainer/VBoxContainer/Score")
	lastscoreLabel = get_node("CenterContainer/VBoxContainer/LastScore")
	highscoreLabel = get_node("CenterContainer/VBoxContainer/HighScore")
	historyBox = get_node("CenterContainer/VBoxContainer/HistoryBox")
	#TODO: Add a legend for the history box

	#Have the New Game button grab focus to assist gamepad and keyboard navigation
	get_node("CenterContainer/VBoxContainer/HBoxContainer/NewGame").grab_focus()

	#Connect signals for both of the buttons that point to functions in this script
	var temp = get_node("CenterContainer/VBoxContainer/HBoxContainer/NewGame").connect("pressed", self, "on_new_game_clicked")
	temp = get_node("CenterContainer/VBoxContainer/HBoxContainer/Menu").connect("pressed", self, "on_menu_clicked")

	#Connect the signal for the history box's draw event to a fucntion that will draw the chart
	temp = historyBox.connect("draw", self, "draw_history")

	#These two lines do nothing
	#Godot marks unused variables as errors. Since type is only used by the bee_handler script, we need to do something with it here to suppress that error.
	if temp:
		pass

	#Release the mouse so that the player can actually click on the buttons \o/
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)


#This function updates all the values that are displayed here
func update_values(pollen, time, stage, score, lastscore, highscore, scoreHistory):
	#Updated all the relevant labels, adding 1 to stage since it's zero based
	stageLabel.set_text("Stage: " + str(stage + 1))
	pollenLabel.set_text("Pollen collected: " + str(pollen))
	timeLabel.set_text("Survived: " + get_parent().get_time_string(time))
	scoreLabel.set_text("Final score: " + str(int(score)))
	lastscoreLabel.set_text("Previous score: " + str(lastscore))
	highscoreLabel.set_text("Best score: " + str(highscore))

	#Set the score history so that we can refer to it later
	history = scoreHistory


#This function draws a chart of historic score values
func draw_history():
	#If there aren't at least two scores in the history, let's skip drawing. Charts with one point are boring
	if history.size() < 2:
		historyBox.visible = false
		return

	#Declare some variables that will help us draw
	var points = PoolVector2Array() #An array of the charted score points to be drawn
	var times = PoolVector2Array() #An array of the charted time points to be drawn
	var maxPoints = 0 #Will track the highest point value
	var maxTime = 0 #Will track the highest time value
	var minPoints = 999999 #Will track the lowest point value - default to a stupidly high number so that it'll never stay at the initial value
	var minTime = 999999 #Will track the lowest time value - default to a stupidly high number so that it'll never stay at the initial value
	var historyOffset = Vector2(10, 10) #Padding around the chart
	var historyStep = (historyBox.get_size().x - historyOffset.x * 2) / get_parent().maxScoreHistory + 1 #How far each subsequent point should move along the X axis
	var historyHeight = historyBox.get_size().y - historyOffset.y * 2 #How much Y axis space is available for charting

	#Work out the highest and lowest times/scores
	for item in history:
		if item[0] > maxPoints:
			maxPoints = item[0]
		if item[0] < minPoints:
			#Subtract 1 here so that the minimum actual value never ends up being zero once the minimum is deducted
			minPoints = item[0] - 1
		if item[1] > maxTime:
			maxTime = item[1]
		if item[1] < minTime:
			#Subtract 1 here so that the minimum actual value never ends up being zero once the minimum is deducted
			minTime = item[1] - 1

	#Calculate X and Y coordinates for each point and time value, with Y values calculated as the portion of historyHeight equal to the value's portion of the difference between the lowest and highest values
	#If that makes sense - in short, it just scales up/down so that as much space is given to the visible value range as possible
	for i in history.size():
		points.push_back(historyOffset + Vector2(i * historyStep, historyHeight - historyHeight / ((maxPoints - minPoints) / (history[i][0] - minPoints))))
		times.push_back(historyOffset + Vector2(i * historyStep, historyHeight - historyHeight / ((maxTime - minTime) / (history[i][1] - minTime))))

	#Draw a coloured line on the history box betwean each point and the next, blue for time, yellow for score
	for i in history.size() - 1:
		historyBox.draw_line(times[i], times[i + 1], Color(0.5, 0.5, 1))
		historyBox.draw_line(points[i], points[i + 1], Color(1, 1, 0))
	#TODO: Draw axes, as well as lines and values for max, min and average
	#TODO: Indicate stage progress in chart, perhaps through line colour, or through grid lines


#This function starts a new game and removes the end game screen from the scene tree
func on_new_game_clicked():
	get_parent().setup()
	queue_free()

#This function shows the main menu and removes the end game screen from the scene tree
func on_menu_clicked():
	get_parent().show_menu()
	queue_free()
