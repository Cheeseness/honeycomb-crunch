extends Spatial

#Declare some flower variables
var size = 1 #The size of this flower (set in hex_spawner.gd)
var moveSpeed = 3 #The movement speed of this flower (set in hex_spawner.gd)
var spinSpeed = 0.5 #The spin speed of this flower (set in hex_spawner.gd)
var type = 1  #Flowers are type 1 for collisions
var camera #A variable that we can access the camera via
var flowerType = 1 #The type of the flower (set in hex_spawner.gd)


func _ready():
	#These two lines do nothing
	#Godot marks unused variables as errors. Since type is only used by the bee_handler script, we need to do something with it here to suppress that error.
	if type:
		pass

	#Add a material override based on flower type so that the flower is coloured correctly
	if flowerType == 2:
		get_node("Petals").material_override = preload("res://models/Petals2.material")
	elif flowerType == 3:
		get_node("Petals").material_override = preload("res://models/Petals3.material")

	#Grab a reference to the camera that we can refer to later
	camera = get_parent().get_node("Camera")

	#Set the scale of the flower
	set_scale(Vector3(size, size, size))


#This built-in function is called regularly by the engine
func _process(delta):
	#Rotate the flower by its spin speed around the Y axis, and backwards (compared to hexes) around the X axis
	rotate_y(spinSpeed * delta)
	rotate_x(spinSpeed * -2 * delta)

	#Move the flower by its move speed
	global_translate(Vector3(0, 0, moveSpeed * delta))

	#Get the position of the flower in screen coordinates and despawn it if it's safely past the bottom edge
	var chicken = camera.unproject_position(get_global_transform().origin)
	if chicken.y > get_viewport().get_visible_rect().size.y + get_parent().hexPadding * camera.size:
		get_parent().remove_flower(self)
