extends Control

#Declare some variables for displaying credits
var creditsFile = "credits.json" #The file that credits are stored in
var headingFont = preload("res://fonts/event_font.tres") #The font that we'll use for headings
var itemFont = preload("res://fonts/hud_font.tres") #The font that we'll use for names/roles
var creditsAnchor #A node that the credits will all be attached to


func _ready():
	#Instantiate a new VBoxContainer and set it to the window size
	creditsAnchor = VBoxContainer.new()
	creditsAnchor.set_size(get_viewport().get_visible_rect().size)

	#Read json from the credits file
	var credits = get_parent().read_json("res://json/" + creditsFile)

	#Add the credits to the credits anchor via a fancy recursive function
	add_credits_item(credits, creditsAnchor, 0)

	#Add the credits anchor to the scene tree and set its position to be just below the window boundary
	add_child(creditsAnchor)
	creditsAnchor.set_position(Vector2(0, get_viewport().get_visible_rect().size.y))


#This built-in function is called regularly by the engine
func _process(delta):
	#Get the position of the credits anchor, and if it's more than its own height above the top, wrap it back to below the window boundary
	var temp = creditsAnchor.get_position()
	if temp.y < - creditsAnchor.get_size().y:
		temp.y = get_viewport().get_visible_rect().size.y

	#Move the credits anchor up a bit
	creditsAnchor.set_position(Vector2(0, temp.y - delta * 50))


#This built-in function is called whenever there are input events that aren't already handled
func _unhandled_input(event):
	#If one of the "ui_cancel" or "ui_accept" inputs are pressed, return to the menu and remove the credits from the scene tree
	if event is InputEventKey:
		if event.is_action_pressed("ui_cancel") || event.is_action_pressed("ui_accept"):
			get_parent().show_menu()
			queue_free()
			get_tree().set_input_as_handled()


#This function recursively adds credits items to the specified credits anchor, with heading padding based on depth
func add_credits_item(item, anchor, depth):
	if item is Dictionary:
		#If the item's a dictionary, that means it's got headings and extra items that we need to call add_credits_item() again for
		for section in item:
			#Instantiate and add a MarginContainer to the anchor before the heading, sized based on depth
			var marginTemp = MarginContainer.new()
			if depth == 0:
				marginTemp.set("rect_min_size", Vector2(10, 50))
			else:
				marginTemp.set("rect_min_size", Vector2(10, 25))
			anchor.add_child(marginTemp)

			#Instantiate a Label for the heading, set its text, font and alignment, then add it to the anchor
			var tempLabel = Label.new()
			tempLabel.set_text(section)
			tempLabel.add_font_override("font", headingFont)
			tempLabel.set_align(Label.ALIGN_CENTER)
			anchor.add_child(tempLabel)

			#Call add_credits_item() again with the items under this heading
			add_credits_item(item[section], anchor, depth + 1)
	else:
		#If the item isn't a dictionary, it's just an array of names/roles that we can add as labels
		for subItem in item:
			#Instantiate a Label for the item, set its text, font and alignment, then add it to the anchor
			var tempLabel = Label.new()
			tempLabel.set_text(subItem)
			tempLabel.add_font_override("font", itemFont)
			tempLabel.set_align(Label.ALIGN_CENTER)
			anchor.add_child(tempLabel)