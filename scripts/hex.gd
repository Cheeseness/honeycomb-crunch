extends Spatial

#Declare hex variables
var size = 1 #The size of this hex (set in hex_spawner.gd)
var moveSpeed = 6 #The movement speed of this hex (set in hex_spawner.gd)
var spinSpeed = 3 #The spin speed of this hex (set in hex_spawner.gd)
var type = 0  #Hexes are type 0 for collisions
var camera #A variable that we can access the camera via

func _ready():
	#These two lines do nothing
	#Godot marks unused variables as errors. Since type is only used by the bee_handler script, we need to do something with it here to suppress that error.
	if type:
		pass

	#Grab a reference to the camera node
	camera = get_parent().get_node("Camera")

	#Set the scale of the hex
	set_scale(Vector3(size, size, size))


#This built-in function is called regularly by the engine
func _process(delta):
	#Rotate the hex by its spin speed
	rotate_x(spinSpeed * delta)

	#Move the hex by its move speed
	global_translate(Vector3(0, 0, moveSpeed * delta))

	#Get the position of the hex in screen coordinates and despawn it if it's safely past the bottom edge
	var chicken = camera.unproject_position(get_global_transform().origin)
	if chicken.y > get_viewport().get_visible_rect().size.y + get_parent().hexPadding * camera.size:
		get_parent().remove_hex(self)