extends RigidBody

#Declare pollen variables
var type = 2 #Pollen is type 2 for collisions
var gravTime = 0.5 #The amount of time that has to pass before the pollen can be collected
var deathTime = 10 #The amount of time that a piece of pollen will hang around for before it disappears
var lifeTime = 0 #How long the pollen has been around for


#This built-in function is called regularly by the engine
func _process(delta):
	#These two lines do nothing
	#Godot marks unused variables as errors. Since type is only used by the bee_handler script, we need to do something with it here to suppress that error.
	if type:
		pass

	#Count up how long the pollen has been around
	lifeTime += delta

	#TODO: Work some collision mask magic to prevent the bee's gravity from affecting pollen during the gravTime gap
	#If enough time has passed, make sure that the physics body is awake and can trigger collision events
	if lifeTime > gravTime:
		can_sleep = false
		sleeping = false

	#TODO: Trigger some visual feedback showing that despawn is imminent
	#If enough time has passed, despawn the pollen
	if lifeTime >= deathTime:
		#TODO: Trigger a nice despawn effect
		get_parent().remove_pollen(self)