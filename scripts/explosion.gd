extends Spatial

#Declare explosion variables
var components = [] #A list of the current particle effects

func _ready():
	#Loop through all child nodes looking for Particles nodes, and append them to our list
	for child in get_children():
		if child is Particles:
			components.append(child)

			#Set the particle system emitting particles
			child.emitting = true

	#Play the boom sound
	get_node("AudioStreamPlayer").play()


#This built-in function is called regularly by the engine
func _process(_delta):
	#If any particle effects are still emitting, wait, otherwise remove the whole lot from the scene tree
	var timeToDie = true
	for component in components:
		if component.emitting == true:
			timeToDie = false
	if timeToDie:
		queue_free()
