extends Control

#Declare some variables that we'll use to keep track of UI elements
var honeycomb #The "Honeycomb" text graphic
var crunch #The "CRUNCH" text graphic
var bee #The cool space bee graphic
var pollens #The glowy pollen graphic
var buttons #The UI node containing the menu buttons
var tween #A tween for animating all the everything
var version #The text label that will display the version number


func _ready():
	#Grab references for all the menu/title screen bits that we care about
	honeycomb = get_node("Honeycomb")
	crunch = get_node("Crunch")
	bee = get_node("Bee")
	pollens = get_node("Pollens")
	buttons = get_node("MarginContainer")
	version = get_node("Version")

	#Update the version label with the version number that's already been loaded
	version.set_text(get_parent().versionNumber)

	#Instantiate the tween and add it to the scene tree
	tween = Tween.new()
	add_child(tween)

	#FIXME: Each tween has a 0.2 second delay as a dodgey workaround to hide a momentary pause when adding all backgrounds
	#Set up a tween to fade in the "honeycomb" text and move it up to its final position
	tween.interpolate_property(honeycomb, "modulate", Color(1, 1, 1, 0), Color(1, 1, 1, 1), 2, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT, 0.2)
	tween.interpolate_property(honeycomb, "rect_position", honeycomb.get_position(), Vector2(0, 0), 6, Tween.TRANS_SINE, Tween.EASE_OUT, 0.2)

	#TODO: Fade the "CRUNCH" text in so that it's not immediately visible on aspect ratios taller than 16:9
	#Set up a tween to drop the "CRUNCH" text in with a nice bounce after 2.5 seconds
	tween.interpolate_property(crunch, "rect_position", crunch.get_position(), Vector2(0, 0), 1.5, Tween.TRANS_BOUNCE, Tween.EASE_OUT, 2.7)

	#Set up a tween to fade in the menu buttons after a second
	tween.interpolate_property(buttons, "modulate", Color(1, 1, 1, 0), Color(1, 1, 1, 1), 3, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT, 1.2)

	#Set up a tween to fade in the pollen graphic after 2 seconds and move it down to its final position
	tween.interpolate_property(pollens, "modulate", Color(1, 1, 1, 0), Color(1, 1, 1, 1), 1, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT, 2.2)
	tween.interpolate_property(pollens, "rect_position", pollens.get_position(), Vector2(0, 0), 6, Tween.TRANS_SINE, Tween.EASE_IN_OUT, 0.2)

	#TODO: Fade the bee in so that it's not immediately visible on aspect ratios taller than 16:9
	#Set up a tween to move the cool space bee up to its final position after 2 seconds
	tween.interpolate_property(bee, "rect_position", bee.get_position(), Vector2(0, 0), 4.5, Tween.TRANS_SINE, Tween.EASE_IN_OUT, 2.2)

	#Start all of those tweens!
	tween.start()

	#If the player has bees left, there must be a game in progress, so make the New Game button say "Resume" instead
	if get_parent().bees > 0:
		get_node("MarginContainer/VBoxContainer/NewGame").set_text("Resume")

	#Have the New Game button grab focus to assist gamepad and keyboard navigation
	get_node("MarginContainer/VBoxContainer/NewGame").grab_focus()

	#Connect signals for each of the menu buttons that point to functions in this script
	var temp = get_node("MarginContainer/VBoxContainer/NewGame").connect("pressed", self, "on_new_game_pressed")
	temp = get_node("MarginContainer/VBoxContainer/Fullscreen").connect("pressed", self, "on_fullscreen_pressed")
	temp = get_node("MarginContainer/VBoxContainer/Credits").connect("pressed", self, "on_credits_pressed")
	temp = get_node("MarginContainer/VBoxContainer/Website").connect("pressed", self, "on_website_pressed")
	temp = get_node("MarginContainer/VBoxContainer/Quit").connect("pressed", self, "on_quit_pressed")

	#These two lines do nothing
	#Godot marks unused variables as errors. Since type is only used by the bee_handler script, we need to do something with it here to suppress that error.
	if temp:
		pass


#This function sets the credits rolling and removes the menu from the scene tree
func on_credits_pressed():
	get_parent().show_credits()
	queue_free()


#This function closes the game
func on_quit_pressed():
	get_tree().quit()


#This function removes the menu from the scene tree and starts a new game or resumes the game in progress
func on_new_game_pressed():
	if get_parent().bees > 0:
		get_parent().unpause()
		queue_free()
	else:
		get_parent().setup()
		queue_free()


#This function toggles fullscreen/windowed mode
func on_fullscreen_pressed():
	if(OS.is_window_fullscreen()):
		get_node("MarginContainer/VBoxContainer/Fullscreen").set_text("Fullscreen")
		OS.set_window_fullscreen(false)
		OS.set_window_maximized(false)
		OS.set_window_size(Vector2(1280, 720))
	else:
		OS.set_window_fullscreen(true)
		get_node("MarginContainer/VBoxContainer/Fullscreen").set_text("Window")


#This function opens the game's website in a browser
func on_website_pressed():
	var url = "http://honeycombcrunch.twolofbees.com"
	print("Attempting to launch URL: ", url)
	print(OS.shell_open(url))