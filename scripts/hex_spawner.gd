# Hi! My name's Cheese, and I wrote all this code.
# 
# As with most creative processes, there are multiple valid approaches
# that can be used to solvea a problem. What you see here is my approach,
# but there are plenty of others!
# 
# For example, I tend to keep my scripts, scenes, sprites, and models in
# separate folders, but another valid and popular approach is to store
# each scene in its own folder along with the scenes, sprites, etc. that
# it makes use of so that those things are more self contained and easier
# to copy between projects.
# 
# I've done my best to thoroughly comment the code here and describe what's
# going on in a way that will hopefully make sense to anybody with a small
# amount of programming experience. That said, I do expect that you'll be
# willing to read up on what various nodes and parts of the Godot API are
# for. Godot's built-in editor allows you to open the declaration or
# documentation for a variable or function by Ctrl + Clicking on it, so be
# sure to make use of that!
# 
# In the interests of not cluttering up the codebase, I haven't imposed line
# length limits on any other comments. If you want to read everything without
# horizontal scrolling, you can enable word wrapping by going to the Editor
# menu and selecting Editor Settings, then navigating to Line Numbers under
# Text Editor, and checking the Word Wrap box.
# 
# There's also a lot going on in the editor tools that I haven't found a
# good way to comment/document. You'll have to discover those by exploring
# and experimenting on your own, or by following more directed tutorials.
# 
# Last, but not least, I've also scattered some "TODO" items around the code
# as exercises for the reader to consider. You can generate a list using the
# editor's "Find in files" dialog from the Search menu.
# 
# I had a lot of fun making this game. I hope you can find some fun in it
# too :)
# 
# Good luck and enjoy!
# 
# - Cheese


extends Spatial

#Bee related declarations!
var bee #The player's bee
var beeScene = preload("res://scenes/bee.tscn") #Scene containing the bee model and logic
var initialDeathTime = 0.25 #The initial amount of time the bee should hang around for after dying
var deathTime = initialDeathTime #The amount of time the bee should hang around for after dying
var isDying = false #Track whether the player is in the middle of a death animation or not
var explosionScene = preload("res://scenes/explosion.tscn") #The scene containing our cool bee explosition and its logic for self-termination
var initialSpawnDelay = 2 #The amount of time between dying and a new bee spawning
var spawnDelay = -1 #Will track the time between dying and a new bee spawning. Starts at -1 so that we can run game stuff behind the main menu without a new bee spawning
var newLifeThreshold = 100 #How much pollen needs to be collected before a new bee is awarded
var initialBees = 3 #How many bees the player starts with
var bees = -1 #How many bees the player currently has

#Background related declarations!
enum Backgrounds {SPACE, SKY, SEA} #An enum of backgrounds used as keys into backgroundList
var backgroundList = { #A list of background scenes and associated environment colours for each stage
		Backgrounds.SPACE: [preload("res://scenes/background_space.tscn").instance(), Color(0.024, 0.027, 0.071)],
		Backgrounds.SKY: [preload("res://scenes/background_sky.tscn").instance(), Color(0.35, 0.35, 0.60)],
		Backgrounds.SEA: [preload("res://scenes/background_sea.tscn").instance(), Color(0.09, 0.40, 0.40)]
	}
var backgroundOrder = [Backgrounds.SPACE, Backgrounds.SKY, Backgrounds.SEA] #The order that backgrounds will appear in
var currentBackgroundIndex = -1 #The index of the currently used background
var backgroundTween #A Tween for transitioning environment colours
var stageCounter #Will count how many stages the player has completed in the current run
var stageDuration = 60 #How long each stage should last
var stageTransitionDuration = 10 #How long stage transitions should take
var stageAccelerationOffset = 0 #Will store how much acceleration is reduced by for the start of each stage

#Declare some arrays to keep track of hexes, flowers and pollen in
#Yes, I know the plural of pollen is pollen. I just enjoy sticking an S on the end
var hexes = []
var flowers = []
var pollens = []

#Decalre some hex related stuff!
var hexScene = preload("res://scenes/hex.tscn") #The scene containing our hexagon and its logic
var hexDelay = 0.5 #The delay between hexes spawning. This is small because it's treated as a divisor
var timeSinceLastHex = 0 #Will track the amount of time that's passed since the last hex was spawned
var hexSpeedJitter = 5 #How much the speed of hexes will vary from each other
var hexSpinJitter = 3 #How much the spin of hexes will vary from each other
var hexSizeJitter = 0.5 #How much the size of hexes will vary from each other
var hexPadding = 2 #How far from the game boundary hexes will spawn
var initialAcceleration = 1.0 #The initial speed boost given to hexes for time passed
var accelerationFactor #Will store a multuplier for the speed boost given to hexes for time passed
var accelerationStep = 0.20 #How much the accelerationFactor should increase by every second

#Declare some flower related stuff
var flowerDelay = 5 #The delay between flowers spawning
var timeSinceLastFlower = 1 #Will track the amount of time that's passed since the last flower spawned. Starts at 1 so that 
var flowerScene = preload("res://scenes/flower.tscn") #The scene containing our flower and its logic
var petalScene = preload("res://scenes/flower_explosion.tscn") #The scene containing our petal explosion effect and its logic for self-terminating

#Declare some pollen related stuff
var pollenScene = preload("res://scenes/pollen.tscn") #The scene containing our pollen and its logic for self-termination
var pollenVelocity = 10 #The initial velocity of new pollen
var pollenSpawnCount = 10 #The amount of pollen that should spawn when a flower is hit
var pollenTimeGap = 0.5 #The amount of time within which more pollen must be collected in order to push the pollen collection sound's pitch up
var pollenSoundPitchStep = 0.5 #The rate by which the pollen collection sound increases/decreases in pitch
var pollenSoundPitchLimit = 12 #The maximum pitch that the pollen collection sound can go through
var timeSinceLastPollen = 0 #Will track the amount of time that has passed since the last piece of pollen was collected
var currentPollenPitch = 0 #Will track the current pitch level of the pollen collection sound
var initialPollen = 0 #The players starting pollen
var pollenCount #The players current pollen
var pollenSound #An AudioStreamPlayer to play the pollen collection sound

#Declare some UI related stuff
var hud #Will keep track of the HUD node
var camera #Will keep track of the HUD node
var hudScene = preload("res://scenes/hud.tscn") #The scene containing the HUD and logic for updating HUD elements
var menuScene = preload("res://scenes/menu.tscn") #The scene containing the main menu/title screen
var creditsScene = preload("res://scenes/credits.tscn") #The scene containing logic for displaying credits
var gameOverScene = preload("res://scenes/game_over.tscn") #The scene containing the end game screen and logic for displaying stats
var score = 0 #The player's current score
var pollenBonus = 8 #The amount of score added per pollen collected
var timeBonus = 6 #The amount of score added per second survived
var scorePath = "user://" #The folder that the score save file will go in
var scoreFile = "scores.json" #The filename of the score save file
var maxScoreHistory = 20 #How much history should be stored in the score save file/displayed on the end game screen
var playTime #Will track how long the current run has lasted
var paused = true #Whether or not gameplay processing should occur
var lastJoypadDeviceID = -1 #Will track the device ID that the last piece of joystick input was received from. This helps us respect input that was given before the current bee spawned
var versionNumber = "Unknown version" #Will store the game's version number


func _ready():
	#Randomise the random number generator so that we don't get the same random numbers every time we run the game
	randomize()

	#Load the version number from file
	load_version_number()

	#Get a reference to the camera node and store it
	camera = get_node("Camera")

	#Create an AudioStreamPlayer to play the pollen collection sound and add it to the scene tree
	pollenSound = AudioStreamPlayer.new()
	pollenSound.set_stream(load("res://sounds/GUI Sound Effects_031.wav"))
	add_child(pollenSound)

	#Instantiate the backgroundTween and add it to the scene tree
	backgroundTween = Tween.new()
	add_child(backgroundTween)

	#Add all of the backgrounds to the scene tree
	for background in backgroundList:
		add_child(backgroundList[background][0])
	set_background(Backgrounds.SPACE)

	#Set the music playing
	get_node("AudioStreamPlayer").play()

	#Display the main menu/title screen
	show_menu()

	#Give the accelerationFactor an initial value so that we can have hexes in the background of the main menu
	accelerationFactor = initialAcceleration


#This function handles transitioning between backgrounds
func set_background(index):
	#Calculate how long parts of the transitions should take
	var fadeDelay = stageTransitionDuration / 4 #How long particle systems should wait before fading in
	var backgroundTransitionDuration = stageTransitionDuration / 2 #How long the background colour should take to tween

	var shouldFade = false
	#If we're not already on the background we're trying to change to, fade out the old background
	if currentBackgroundIndex != index:
		shouldFade = true
		if currentBackgroundIndex != -1:
			backgroundList[currentBackgroundIndex][0].fade_out(stageTransitionDuration / 2)
		else:
			#If we're coming in from the main menu, don't bother delaying the fade in of particles, and make the tween duration shorter so that we can get to gameplay quicker
			fadeDelay = 0
			backgroundTransitionDuration = stageTransitionDuration / 4
	#Tween the background colour. Hexes and flowers won't spawn till this tween is done
	backgroundTween.interpolate_property(camera.environment, "background_color", camera.environment.background_color, backgroundList[index][1], backgroundTransitionDuration, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	backgroundTween.start()

	#Fade in the new background's particle systems if needed (if we're not already on the right background)
	if shouldFade:
		backgroundList[index][0].fade_in(stageTransitionDuration / 2, fadeDelay)

	#Update the current background index to the new background
	currentBackgroundIndex = index


#This function configures/resets everything needed for the start of a new run
func setup():
	print("Setting up new game")

	#Set the background to the first background in our list
	set_background(backgroundOrder[0])

	#Set initial values for a bunch of things
	accelerationFactor = initialAcceleration
	stageAccelerationOffset = 0
	stageCounter = 0
	pollenCount = initialPollen
	playTime = 0
	bees = initialBees
	score = 0
	spawnDelay = initialSpawnDelay

	#Create a new instance of the HUD scene and add it to the scene tree
	hud = hudScene.instance()
	add_child(hud)

	#Add bee counters to the HUD
	for _i in range(bees):
		hud.add_bee()

	#Update the HUD's timer and pollen count
	hud.update_time(get_time_string(playTime))
	hud.update_pollen(pollenCount)

	#Flash the current stage on screen. No delay here since we're not waiting for a background transition
	hud.show_stage(stageCounter)

	#Clean up any spawned hexes, flowers or pollen
	for hex in hexes:
		hex.queue_free()
	hexes.clear()
	for flower in flowers:
		flower.queue_free()
	flowers.clear()
	for pollen in pollens:
		pollen.queue_free()
	pollens.clear()

	#Grab the mouse
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	print("Done setting up")


#This function shows the main menu/title screen
func show_menu():
	#Add an instance of the menu to the scene tree
	add_child(menuScene.instance())


#This built-in function is called whenever there are input events that aren't already handled
func _unhandled_input(event):
	#If one of the "ui_cancel" inputs is pressed, pause the game
	if event is InputEventKey:
		if event.is_action_pressed("ui_cancel"):
			if !get_tree().paused:
				pause()
			else:
				unpause()
			get_tree().set_input_as_handled()

#This function pauses the game
func pause():
	#Release the mouse cursor
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

	#Show the menu
	show_menu()

	#Pause the scene tree
	get_tree().paused = true


#This function unpauses the game and is called by the main menu when the resume button is clicked
func unpause():
	#Release grab the mouse cursor
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

	#Unpause the scene tree
	get_tree().paused = false


#This built-in function is called regularly by the engine
func _process(delta):
	#If the game isn't paused, increment the timer and update the HUD
	if !paused:
		playTime += delta
		hud.update_time(get_time_string(playTime))

		#Update the acceleration factor
		accelerationFactor = accelerationStep * playTime / 6

		#Check whether or not enough tiem has passed for a stage change, and if so increase the stage number
		#Thanks to modulo division, stageTemp will always be a valid index for backgroundOrder
		var stageTemp = int(playTime / stageDuration) % backgroundOrder.size()
		if stageTemp != backgroundOrder.find(currentBackgroundIndex):
			stageCounter += 1

			#Reduce acceleration a bit, so that the next stage starts a little easier than the last stage ended
			stageAccelerationOffset = stageCounter * accelerationStep * 5

			#Change the background to the new
			set_background(backgroundOrder[stageTemp])

			#Flash the current stage on screen
			hud.show_stage(stageCounter, stageTransitionDuration / 4)

	#Increment the time since pollen was last collected
	timeSinceLastPollen += delta

	#If we're dying, keep count down how long we've been dying for
	if isDying:
		deathTime = deathTime - delta
		#If we've waited long enough, free the dead bee, reset deathTime and pause the game
		if deathTime <= 0:
			bee.queue_free()
			bee = null
			isDying = false
			deathTime = initialDeathTime
			paused = true

			#If there are still bees left, set the spawn delay so that a new bee can spawn
			if bees > 0:
				spawnDelay = initialSpawnDelay
			else:
				#if there are no bees left, instantiate and add the end game scene to the scene tree 
				var gameOverScreen = gameOverScene.instance()
				add_child(gameOverScreen)

				#Calculate the score for the run
				var currentScore = int(pollenCount * pollenBonus + playTime * timeBonus)

				#Load old scores from file and add the current score to it
				var oldScores = load_old_scores()
				oldScores["scoreHistory"].append([int(currentScore), playTime])

				#Prune some scores if we've got too many, removing the front (oldest) ones first
				while oldScores["scoreHistory"].size() > maxScoreHistory:
					oldScores["scoreHistory"].pop_front()

				#Give the end game scene some stats ot display
				gameOverScreen.update_values(pollenCount, playTime, stageCounter, currentScore, oldScores["lastscore"], max(oldScores["bestscore"], currentScore), oldScores["scoreHistory"])

				#Save scores out to file
				save_new_scores(currentScore, oldScores)

				#Set bees to -1 so that we can continue to run the game in the background without the end game scene being shown again
				bees = -1

				#Get rid of the HUD
				hud.queue_free()

	#Count down how long we've been waiting to spawn if the spawn delay is above zero
	if spawnDelay >= 0:
		spawnDelay = spawnDelay - delta
		#If this pass through _process() puts us below zero, instantiate a new bee and add it to the scene tree
		if spawnDelay <= 0:
			bee = beeScene.instance()
			add_child(bee)

			#Work out where the middle of the screen is in 3D space and move the bee to that position using global_translate()
			var spawnOffset = get_viewport().get_visible_rect().size
			spawnOffset.x = spawnOffset.x / 2
			spawnOffset.y = spawnOffset.y
			var spawnPos = actually_useful_project_position(spawnOffset, camera.translation.y / 2)
			bee.global_translate(spawnPos)

			#Tell the bee to track the height that it spawned at so that we can smoothly drop into the play area
			bee.spawnHeight = spawnPos.z

			#Unpause, remove a bee, and update the HUD
			paused = false
			bees -= 1
			hud.remove_bee()

	#Count up the time since the last hex spanwned and spawn one if enough time has passed (and if we're not transitioning to a new stage)
	timeSinceLastHex = timeSinceLastHex + delta
	if timeSinceLastHex >= hexDelay / (accelerationFactor - stageAccelerationOffset) && !backgroundTween.is_active():
		timeSinceLastHex = 0
		add_hex()

	#Count up the time since the last flower spanwned and spawn one if enough time has passed (and if we're not transitioning to a new stage)
	timeSinceLastFlower = timeSinceLastFlower + delta
	if timeSinceLastFlower >= flowerDelay && !backgroundTween.is_active():
		timeSinceLastFlower = 0
		add_flower()


#This function is a hacky workaround for a Camera.project_position() not taking a depth parameter.
#This behaviour will be adjusted in Godot 3.2 onwards, making this function redundant https://github.com/godotengine/godot/pull/29248
func actually_useful_project_position(position, depth):
	#It's undocumented at the time of writing, but the position of the camera's near clipping plane is used as the depth, so let's keep track of what that is
	var oldNear = camera.get_znear()

	#Temporarily set the distance of the near camera's clipping plane to the depth we want, do the calculation, then reset the near clipping plane
	camera.set_znear(depth)
	var temp = camera.project_position(position)
	camera.set_znear(oldNear)

	#Return the projected position with the desired depth
	return temp


#This function generates a string representing time as minutes, seconds, and milliseconds, with colon sepators
func get_time_string(time):
	var milliseconds = str(int(time * 100) % 100)
	time = int(time)
	var seconds = str(time % 60)
	time = time - (time % 60)
	var minutes = str(time / 60)
	return minutes.pad_zeros(2) + ":" + seconds.pad_zeros(2) + ":" + milliseconds.pad_zeros(2)

#This function loads the version number from version.txt if it exists in the resource bundle/path
func load_version_number():
	var versionPath = "res://version.txt"
	var file = File.new()
	if not file.file_exists(versionPath):
		print("ERROR: Unable to load version")
		return
	file.open(versionPath, file.READ)
	versionNumber = file.get_as_text()
	file.close()


#This function reads and parses json from the specified file path
func read_json(path):
	var file = File.new()
	if not file.file_exists(path):
		print("ERROR: Unable to open resource ", path)
		return null
	file.open(path, file.READ)
	var text = file.get_as_text()
	file.close()
	var parse = JSON.parse(text)
	if parse.error == OK:
		return parse.result
	else:
		print("Error reading json at line " + str(parse.error_line) + ": " + parse.error_string)
		return null


#This function reads saved scores if they exist, and creates and empty score data structure if they don't
func load_old_scores():
	var oldScores = read_json(scorePath + scoreFile)
	if oldScores == null:
		oldScores = {"lastscore": 0, "bestscore": 0, "scoreHistory": []}
	if !("scoreHistory" in oldScores):
		oldScores["scoreHistory"] = []
	return oldScores

#This function saves scores out to file
func save_new_scores(newScore, oldScores):
	#Update the last score and best score values as needed
	oldScores["lastscore"] = int(newScore)
	oldScores["bestscore"] = max(oldScores["bestscore"], oldScores["lastscore"])

	#Declare some utility variables for manipulating the filesystem
	var save_folder = Directory.new()
	var save_game = File.new()

	#If the save path doesn't exist, make it
	if !save_folder.dir_exists(scorePath):
		if save_folder.make_dir_recursive(scorePath) != OK:
			var message = "Unable to make save folder? " + scorePath
			print(message)

	#Open the score file for writing
	if save_game.open(scorePath + scoreFile, File.WRITE) != OK:
		var message = "Unable to open save file " + scorePath + scoreFile + " for writing"
		print(message)
		return

	#Write the scores into the file and close it
	save_game.store_string(to_json(oldScores))
	save_game.close()


#This function instantiates the credits scene and adds it to the scene tree
func show_credits():
	add_child(creditsScene.instance())


#This function displays the bee explosion effect
func bee_boom():
	#Don't do this if we're already dying - one explosion is enough
	if !isDying:
		isDying = true

		#Instantiate the explosion scene, add it to the scene tree, and move it to the bee's position
		var boom = explosionScene.instance()
		add_child(boom)
		boom.set_global_transform(bee.get_global_transform())


#This function handles updating pollen counts and removing collected pollen from the scene tree
func collect_pollen(pollen):
	#If this pollen was collected quickly enough after the last piece, increase the pollen pitch, otherwise lower the pollen pitch
	if timeSinceLastPollen < pollenTimeGap:
		currentPollenPitch = min(currentPollenPitch + pollenSoundPitchStep, pollenSoundPitchLimit)
	else:
		currentPollenPitch = max(1.0, currentPollenPitch - int(timeSinceLastPollen / pollenTimeGap) * 4)

	#The last time pollen was collected is now!
	timeSinceLastPollen = 0

	#Set the appropriate pitch and play the pollen collection sound
	pollenSound.set_pitch_scale(currentPollenPitch)
	pollenSound.play()

	#Increase the pollen count and update the HUD
	pollenCount += 1
	hud.update_pollen(pollenCount)

	#If we've collected enough pollen for a new life, add another bee and update the HUD
	if pollenCount % newLifeThreshold == 0:
		bees += 1
		hud.add_bee(true)

	#Remove the pollen from the scene tree
	remove_pollen(pollen)


#This function adds pollen to the scene tree, and removes the flower it came from
func add_pollen(position, flowerType):
	#Instantiate the flower explosion effect, add it to the scene tree, set its type so that it'll be the right colour, and move it to the flower's position
	var boom = petalScene.instance()
	boom.flowerType = flowerType
	add_child(boom)
	boom.set_global_transform(position)

	#Instantiate as many pollen as this flower type should have and add them all to the scene tree at the flower's position
	for _i in range(pollenSpawnCount * flowerType):
		var pollenInstance = pollenScene.instance()
		add_child(pollenInstance)
		pollenInstance.translate(position.origin)

		#Give the pollen some lightly random velocity
		var velocity = Vector3(min(0.5,  randf()) * pollenVelocity, 0, min(0.5, randf()) * pollenVelocity)
		pollenInstance.set_linear_velocity(velocity)

		#Add the pollen to the pollen list so that we can keep track of it
		pollens.append(pollenInstance)


#This function removes pollen from the pollen list and the scene tree
func remove_pollen(pollen):
	if pollen in pollens:
		pollens.erase(pollen)
		pollen.queue_free()


#This function adds a flower to the scene tree
func add_flower():
	#Instantiate our new flower
	var flowerInstance = flowerScene.instance()

	#Determine the flower's type. 5 in 10 chance for red, 3 in 10 for purple, and 2 in 10 for blue
	var typeTemp = randi() % 10
	if typeTemp > 5:
		flowerInstance.flowerType = 2
		if typeTemp > 8:
			flowerInstance.flowerType = 3

	#Add the flower to the scene tree and add it to our flower list
	add_child(flowerInstance)
	flowers.append(flowerInstance)

	#Give the flower a random starting point within the visible area minus some padding
	var spawnOffset = hexPadding + randi() % int(get_viewport().get_visible_rect().size.x - hexPadding)
	flowerInstance.global_translate(actually_useful_project_position(Vector2(spawnOffset, -hexPadding * camera.size), camera.translation.y))


#This function removes a flower from the scene tree and our flower list
func remove_flower(flower):
	if flower in flowers:
		flowers.erase(flower)
		flower.queue_free()


#This function adds a hex to the scene tree
func add_hex():
	#TODO: Gracefully handle resolutions wider than 16:9 (wide enough, and it takes impossibly long to traverse the play space to collect flowers/pollen)

	#Instantiate our new hex
	var hexInstance = hexScene.instance()

	#Set lightly randomised movement speed, spin speed, and size for the hex
	hexInstance.moveSpeed = (hexInstance.moveSpeed - hexSpeedJitter / 2) + (randf() * hexSpeedJitter) * (accelerationFactor - stageAccelerationOffset)
	hexInstance.spinSpeed = (hexInstance.spinSpeed - hexSpinJitter / 2) + (randf() * hexSpinJitter)
	hexInstance.size = (hexInstance.size - hexSizeJitter / 2) + (randf() * hexSizeJitter)

	#Add the hex to the scene tree and our hex list
	add_child(hexInstance)
	hexes.append(hexInstance)

	#Give the hex a random starting point within the visible area minus some padding
	var spawnOffset = randi() % int(get_viewport().get_visible_rect().size.x)
	hexInstance.global_translate(actually_useful_project_position(Vector2(spawnOffset, -hexPadding * camera.size), camera.translation.y))


#This function removes a hex from the scene tree and our hex list
func remove_hex(hex):
	if hex in hexes:
		hexes.erase(hex)
		hex.queue_free()