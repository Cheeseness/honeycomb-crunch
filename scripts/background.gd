extends Spatial

#Declare some background variables!
var fadeTween #A tween to handle particle effect fades
var normalAlbedoColours = [] #An array of colours for particle effects
var transparentAlbedoColours = [] #A corresponding array of 100% transparent colours for particle effects


func _ready():
	#Instantiate the fade tween and add it to the scene tree
	fadeTween = Tween.new()
	add_child(fadeTween)

	#Loop through all child nodes looking for Particles nodes
	for child in get_children():
		if child is Particles:
			#Grab a copy of the current colour (as defined in the editor) and add it to our list
			normalAlbedoColours.append(child.draw_pass_1.material.albedo_color)

			#Add a copy of that with its alpha to zero to our transparent colours list
			transparentAlbedoColours.append(Color(normalAlbedoColours.back().g, normalAlbedoColours.back().b, normalAlbedoColours.back().r, 0))

			#Set the particle effect's colour to the transparent colour we just made so that it's ready to fade in
			child.draw_pass_1.material.albedo_color = transparentAlbedoColours.back()


#This function fades out particle effets over the specified duration after the specified delay
func fade_out(duration, delay = 0):
	#Declare a variable for us to index into the colour arrays with
	var colourIndex = 0

	#Loop through all the particle effect nodes, and set up a tween from the effect's colour to the transparent version of that colour
	for child in get_children():
		if child is Particles:
			fadeTween.interpolate_property(child.draw_pass_1.material, "albedo_color", normalAlbedoColours[colourIndex], transparentAlbedoColours[colourIndex], duration, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT, delay)
			colourIndex += 1

	#Start all those tweens
	fadeTween.start()


#This function fades in particle effets over the specified duration after the specified delay
func fade_in(duration, delay =  0):
	#Declare a variable for us to index into the colour arrays with
	var colourIndex = 0

	#Loop through all the particle effect nodes, and set up a tween from the transparent colour to the effect's normal colour
	for child in get_children():
		if child is Particles:
			fadeTween.interpolate_property(child.draw_pass_1.material, "albedo_color", transparentAlbedoColours[colourIndex], normalAlbedoColours[colourIndex], duration, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT, delay)
			colourIndex += 1

	#Start all these tweens
	fadeTween.start()