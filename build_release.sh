#!/bin/bash

#Declare and fill some variables
PROJECT_NAME="honeycomb-crunch"
PROJECT_PATH="."
VERSION_NUMBER=$(git describe --abbrev=8 --dirty --always --tags --long)
VERSION_NAME=$(git describe --tags --abbrev=0)
BUILD_PATH_BASE="../builds"
BUILD_DATE=$(date +%Y-%m-%d)
GODOT_PATH="~/.steam/steam/steamapps/common/Godot\ Engine/godot.x11.opt.tools.64"
EXPORT_PROFILE_LINUX="Linux/X11"
EXPORT_PROFILE_MAC="Mac"
EXPORT_PROFILE_WINDOWS="Windows"

#Make a file with the current git commit and tag - the game loads this at runtime
rm -f ./version.txt
echo ${VERSION_NUMBER} > version.txt

echo "Exporting ${PROJECT_NAME}"

#Make dated folders with subfolders for each platform+version
mkdir -p ${BUILD_PATH_BASE}/${BUILD_DATE}/${PROJECT_NAME}_linux_${VERSION_NAME}/
mkdir -p ${BUILD_PATH_BASE}/${BUILD_DATE}/${PROJECT_NAME}_mac_${VERSION_NAME}/
mkdir -p ${BUILD_PATH_BASE}/${BUILD_DATE}/${PROJECT_NAME}_windows_${VERSION_NAME}/

#Export builds for each platform
eval "${GODOT_PATH}" --path ${PROJECT_PATH} --export "${EXPORT_PROFILE_LINUX}" ${BUILD_PATH_BASE}/${BUILD_DATE}/${PROJECT_NAME}_linux_${VERSION_NAME}/${PROJECT_NAME}_linux_${VERSION_NAME}.x86_64 --no-window
eval "${GODOT_PATH}" --path ${PROJECT_PATH} --export "${EXPORT_PROFILE_MAC}" ${BUILD_PATH_BASE}/${BUILD_DATE}/${PROJECT_NAME}_mac_${VERSION_NAME}/${PROJECT_NAME}_mac_${VERSION_NAME} --no-window
eval "${GODOT_PATH}" --path ${PROJECT_PATH} --export "${EXPORT_PROFILE_WINDOWS}" ${BUILD_PATH_BASE}/${BUILD_DATE}/${PROJECT_NAME}_windows_${VERSION_NAME}/${PROJECT_NAME}_windows_${VERSION_NAME}.exe --no-window

echo "Done exporting ${PROJECT_NAME}"
echo "Compressing ${PROJECT_NAME}"

#cd into the dated folder
cd ${BUILD_PATH_BASE}/${BUILD_DATE}/

#Compress/copy archives of the build somewhere convenient
tar cvzf ${PROJECT_NAME}_linux_${VERSION_NAME}_${BUILD_DATE}.tar.gz ${PROJECT_NAME}_linux_${VERSION_NAME}
cp ${PROJECT_NAME}_mac_${VERSION_NAME}/${PROJECT_NAME}_mac_${VERSION_NAME} ./${PROJECT_NAME}_mac_${VERSION_NAME}_${BUILD_DATE}.zip
mkdir -p "Honeycomb CRUNCH.app/Contents/Resources"
cp ../../HoneycombCrunch/icon.icns "Honeycomb CRUNCH.app/Contents/Resources" 
zip -r ${PROJECT_NAME}_mac_${VERSION_NAME}_${BUILD_DATE}.zip "Honeycomb CRUNCH.app/Contents/Resources/icon.icns"
zip -r ${PROJECT_NAME}_windows_${VERSION_NAME}_${BUILD_DATE}.zip ${PROJECT_NAME}_windows_${VERSION_NAME}

echo "Done compressing ${PROJECT_NAME}"

if ! [ -z ${ITCH_DEPLOY} ]
	then
		echo "Deploying ${PROJECT_NAME}"

		#Maybe throw all that up to itch :)
		BUTLER_PATH="/media/stuff/binaries/installers/linux/utilities/itch/butler-linux-amd64/butler"
		ITCH_CHANNEL_LINUX="linux"
		ITCH_CHANNEL_MAC="mac"
		ITCH_CHANNEL_WINDOWS="windows"

		#eval ${BUTLER_PATH} push ${PROJECT_NAME}_linux_${VERSION_NAME}_${BUILD_DATE}.tar.gz cheeseness/${PROJECT_NAME}:${ITCH_CHANNEL_LINUX}-${VERSION_NAME} --userversion ${VERSION_NUMBER}
		eval ${BUTLER_PATH} push ${PROJECT_NAME}_linux_${VERSION_NAME} cheeseness/${PROJECT_NAME}:${ITCH_CHANNEL_LINUX}-${VERSION_NAME} --userversion ${VERSION_NUMBER}
		eval ${BUTLER_PATH} push ${PROJECT_NAME}_mac_${VERSION_NAME}_${BUILD_DATE}.zip cheeseness/${PROJECT_NAME}:${ITCH_CHANNEL_MAC}-${VERSION_NAME} --userversion ${VERSION_NUMBER}
		eval ${BUTLER_PATH} push ${PROJECT_NAME}_windows_${VERSION_NAME}_${BUILD_DATE}.zip cheeseness/${PROJECT_NAME}:${ITCH_CHANNEL_WINDOWS}-${VERSION_NAME} --userversion ${VERSION_NUMBER}

		echo "Done deploying ${PROJECT_NAME}"
fi
